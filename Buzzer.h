#pragma once

class Buzzer: public Pin {
  // no members
  public:
    Buzzer() {}

    Buzzer(int pin_nr) : Pin(pin_nr){
      pinMode(this->pin_nr, OUTPUT);
    }

   void turn_on(){
      digitalWrite(this->pin_nr, LOW);
   }

   void turn_off() {
      digitalWrite(this->pin_nr, HIGH);
   }
};
