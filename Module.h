#pragma once
#include "Range.h"

/**
 * A module represettings the settings at a given time.
 */
class Module {
  private:
    Range* ranges[4];
  public:
    static const int UNDER = -1;
    static const int IN = 0;
    static const int OVER = 1;
  public:
    Module(){}
    
    Module(int r0l, int r0h, int r1l, int r1h, int r2l, int r2h, int r3l, int r3h){
      ranges[0] = new Range(r0l, r0h);
      ranges[1] = new Range(r1l, r1h);
      ranges[2] = new Range(r2l, r2h);
      ranges[3] = new Range(r3l, r3h);
    }

};
