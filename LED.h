#pragma once

#include "Pin.h"

class LED: public Pin{
  private:
    bool led_on = false;
  // No 
  public:
    LED() : Pin(0){
      this->led_on = false;  
    }
    
    LED(int pin_nr) : Pin(pin_nr){
      pinMode(this->pin_nr, OUTPUT);
      this->led_on = false;
    }

    void turn_on(){
      if (!led_on) {
        this->led_digital_write(HIGH);
        led_on = true;
      }
    }

    void turn_off(){
      if (led_on){
        this->led_digital_write(LOW);  
        led_on = false;
      }
    }


    void led_digital_write(int value){
      digitalWrite(this->pin_nr, value);
    }
};
