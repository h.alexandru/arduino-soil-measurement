#pragma once

class Pin {
  protected:
    int pin_nr;
    
  public:
    Pin(){
      this->pin_nr = 0;
    }
    
    Pin(int pin_nr){
      this->pin_nr = pin_nr;  
    }

    int get_pin_nr(){
      return this->pin_nr;
    }
};
