#pragma once

class Range{
  private:
    int min_value;
    int max_value;
  public:
    Range(){
      min_value = 0;
      max_value = 0;
    }
  
    Range(int min_value, int max_value){
      this->min_value = min_value;
      this->max_value = max_value;
    }

    bool check(int value){
      return (min_value <= value && value <= max_value);
    }

    bool isUnderLimit(int value){
      return (value < min_value);
    }

    bool isOverLimit(int value){
      return (max_value < value);
    }
    
};
