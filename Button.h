#pragma once
#include "Pin.h"

class Button: public Pin{
  private:
    bool pressed = false;
    bool released = true;
    int last_state = 0; // We begin with button unpressed
  public:
    Button():Pin(){}

    Button(int pin_nr):Pin(pin_nr){
      pinMode(pin_nr, INPUT);
    }

    void update(){
      if (digitalRead(this->pin_nr) != last_state) {
          last_state = !last_state;
          
          if (last_state == HIGH && !pressed){
                pressed = true;
          }
          
          if (last_state == LOW && !released) {
              released = true;
          }
    }

    bool consume() {
        if (pressed && released) {
            pressed = false; //consume the event
            released = false;
            return true;
        }
        return false;
    }
};
